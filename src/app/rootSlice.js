import { createSlice } from '@reduxjs/toolkit';
import { push } from 'connected-react-router';
import { message } from 'antd';
import { apiRequest } from '../utils/Api';

export const rootSlice = createSlice({
  name: 'root',
  initialState: {
    signInLoading: false,
    signUpLoading: false,
    currentUser: {},
    updateProfileLoading: false,
  },
  reducers: {
    login: (state, action) => {
      state.signInLoading = action.payload;
    },
    setSignUpLoading: (state, action) => {
      state.signUpLoading = action.payload;
    },
    setUpdateProfileLoading: (state, action) => {
      state.updateProfileLoading = action.payload;
    },
    getCurrentUser: (state, action) => (
      {
        ...state,
        currentUser: {
          ...action.payload.data,
        },
      }
    ),
  },
});

export const { login, setSignUpLoading, getCurrentUser, setUpdateProfileLoading } = rootSlice.actions;

export const loginRequest = (data) => (dispatch) => {
  const params = {
    identifier: data.accUsername,
    password: data.accPassword,
  };

  dispatch(login(true));
  apiRequest('/auth/local', 'POST', params).then((response) => {
    dispatch(login(false));
    localStorage.setItem('token', response.data.jwt);
    localStorage.setItem('userId', response.data.user.id);
    dispatch(push('/profile'));
  }).catch((error) => {
    message.error('Некоретно введені дані.');
    dispatch(login(false));
  });
};

export const SignUpRequest = (data) => (dispatch) => {
  dispatch(setSignUpLoading(true));
  apiRequest('/auth/local/register', 'POST', data).then((response) => {
    dispatch(setSignUpLoading(false));
    localStorage.setItem('token', response.data.jwt);
    localStorage.setItem('userId', response.data.user.id);
    dispatch(push('/profile'));
  }).catch((error) => {
    dispatch(setSignUpLoading(false));
    message.error('Некоретно введені дані.');
  });
};

export const getCurrentUserRequest = () => (dispatch) => {
  dispatch(setSignUpLoading(true));
  apiRequest('/users/me', 'GET').then((response) => {
    dispatch(getCurrentUser(response));
  }).catch((error) => {
    message.error('ось пішло не так');
  });
};

export const updateProfileRequest = (id, data, history) => (dispatch) => {
  dispatch(setUpdateProfileLoading(true));
  apiRequest(`/users/${localStorage.userId || id}`, 'PUT', data).then((response) => {
    dispatch(setUpdateProfileLoading(false));
    dispatch(getCurrentUser(response));
    // history.push('/profile');
    message.success('Профіль успішно оновлено');
  }).catch((error) => {
    message.error('ось пішло не так');
  });
};

export const uploadPic = (id, data, history) => (dispatch) => {
  apiRequest(`/upload`, 'POST', data, true, 'multipart/form-data').then((response) => {
    dispatch(updateProfileRequest(id, {
      avatar: response.data[0].id,
    }, history))
  }).catch((error) => {
    message.error('ось пішло не так');
  });
};

export const selectIsLogedIn = (state) => state.root.signInLoading;
export const selectSignUpLoading = (state) => state.root.signUpLoading;
export const selectCurrentUserInfo = (state) => state.root.currentUser;
export const selectUpdateProfileLoading = (state) => state.root.updateProfileLoading;

export default rootSlice.reducer;
