import { configureStore } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import counterReducer from '../features/HomePage/counterSlice';
import rootReducer from './rootSlice';
import bookingReducer from '../features/Booking/bookingSlice';
import movieDetailReducer from '../features/MovieDetail/movieDetailSlice';
import profileReducer from '../features/Profile/profileSlice';

export const history = createBrowserHistory();


export default configureStore({
  reducer: {
    homePage: counterReducer,
    root: rootReducer,
    booking: bookingReducer,
    movieDetail: movieDetailReducer,
    profile: profileReducer,
    router: connectRouter(history),
  },
});
