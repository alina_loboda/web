import React, { useRef, useState } from 'react';
import {
  Comment, Avatar, Form, Button, List, Input,
} from 'antd';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { getAvatarFullUrl } from '../../utils';

const { TextArea } = Input;

const CommentList = ({ comments }) => (
  <List
    dataSource={comments}
    header={`${comments.length} ${comments.length > 1 ? 'replies' : 'reply'}`}
    itemLayout="horizontal"
    renderItem={(props) => <Comment {...props} />}
  />
);

const Editor = ({
  onSubmit, submitting, form,
}) => (
  <Form
    form={form}
    name="comment_form"
    onFinish={onSubmit}
  >
    <Form.Item
      name="comment"
      rules={[{ required: true, message: 'Будь ласка, введіть коментарій.' }]}
    >
      <TextArea rows={4} />
    </Form.Item>
    <Form.Item>
      <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
        Додати коментар
      </Button>
    </Form.Item>
  </Form>
);

const CommentListView = ({ comments, addComment, loading }) => {
  const currentUserAvatar = useSelector((state) => state.root?.currentUser?.avatar?.url);
  const formRef = useRef(null);
  const [form] = Form.useForm();
  const handleSubmit = (e) => {
    e?.comment && addComment(e.comment, form);
  };

  const commentsData = comments.map((c) => (
    {
      author: `${c.user?.firstName || ''} ${c.user?.lastName || ''}`,
      avatar: getAvatarFullUrl(c.user?.avatar?.url),
      content: <p>{c.body}</p>,
      datetime: moment().fromNow(),
    }
  ));
  return (
    <div>
      <Comment
        avatar={(
          <Avatar
            src={getAvatarFullUrl(currentUserAvatar)}
          />
                      )}
        content={(
          <Editor
            onSubmit={handleSubmit}
            submitting={loading}
            form={form}
          />
                      )}
      />
      {comments.length > 0 && <CommentList comments={commentsData} />}

    </div>
  );
};

export default CommentListView;
