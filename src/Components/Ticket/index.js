import React from 'react';
import { Button, Card } from 'antd';
import moment from 'moment';
import 'moment/locale/uk';

import './style.scss';
import { getAvatarFullUrl } from '../../utils';

const { Meta } = Card;

const renderDescription = (seanceStart, selectedPlaces) => (
  <>
    <div>
      Час:
      {moment(seanceStart).format('hh:mm')}
    </div>
    <p>
      Дата:
      {moment(seanceStart).format('DD-MM-YYYY')}
    </p>
    {
              selectedPlaces && selectedPlaces.map((s, i) => (
                <div key={s.id}>
                  Ряд:
                  {' '}
                  {s.row}
                  {' '}
                  Місце:
                  {' '}
                  {s.number}
                </div>
              ))
          }
  </>
);

const Ticket = ({
  movie, seance, selectedPlaces, handlePay,
}) => (
  <Card
    hoverable
    style={{ width: 240 }}
    cover={<img alt="example" src={getAvatarFullUrl(movie.poster.url)} />}
  >
    <Meta
      title={movie.title}
      description={seance && renderDescription(seance.seanceStart, selectedPlaces)}
    />
    {selectedPlaces.length > 0
      && (
      <Button onClick={handlePay} style={{marginTop: 10}}>
        Оплатити
          {' '}
        {selectedPlaces.length * 60}
        {' '}
        грн
        {' '}
      </Button>
      ) }
  </Card>
);

export default Ticket;
