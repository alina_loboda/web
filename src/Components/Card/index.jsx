import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
import classNames from 'classnames';
import { StarFilled } from '@ant-design/icons';
import { BASE_API_URL } from '../../utils/Constants';
import { renderGenres } from '../../utils';
import './style.scss';

const { Title } = Typography;


const MovieCard = ({
  src,
  title,
  showHeaderExtra,
  heightLimited,
  genres,
  rating,
  year,
  duration,
  ageRating,
  hadleClick,
  wide,
}) => (
  <div className={classNames('movie-card', { 'height-limited': heightLimited, wide })} onClick={hadleClick}>
    <img className="movie-card-cover" src={BASE_API_URL + src} />
    <div className="movie-card-info">
      <div className="movie-card-header">
        <Title level={3}>{title}</Title>
        {showHeaderExtra
            && (
            <div className="movie-card-header-extra">
              <span>
                {duration}
                {' '}
                min
              </span>
              <span>{ageRating}</span>
            </div>
            )}
      </div>
      <div className="movie-card-subheader">
        <span>
          {' '}
          {year}
          {' '}
        </span>
        <span>
          {
            genres && renderGenres(genres, '| ')
          }
        </span>
        {rating &&
          <div className="rating">
            <StarFilled />
            <span>{rating}</span>
          </div>
        }
      </div>
    </div>
  </div>
);

MovieCard.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  year: PropTypes.string.isRequired,
  ageRating: PropTypes.string.isRequired,
  showHeaderExtra: PropTypes.bool,
  heightLimited: PropTypes.bool,
  wide: PropTypes.bool,
  duration: PropTypes.number,
  hadleClick: PropTypes.func,
  genres: PropTypes.array,
  rating: PropTypes.string,
};

MovieCard.defaultProps = {
  showHeaderExtra: false,
  heightLimited: false,
  wide: false,
  duration: 0,
  hadleClick: () => {},
  genres: [],
  rating: '',
};

export default MovieCard;
