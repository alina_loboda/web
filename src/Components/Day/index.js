import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classNames from 'classnames';
import './style.scss';

const Day = ({
  date, isActive, handleActive, dayClassName,
}) => (
  <>
    {/* eslint-disable-next-line max-len */}
    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
    <div
      className={classNames('today', { active: isActive }, dayClassName)}
      onClick={handleActive}
    >
      <div className="top-dot"> </div>
      <div className="day-number">{moment(date).format('DD')}</div>
      <div className="day-name">{moment(date).format('ddd')}</div>
    </div>
  </>
);

Day.propTypes = {
  date: PropTypes.string.isRequired,
  dayClassName: PropTypes.string,
  handleActive: PropTypes.func.isRequired,
  isActive: PropTypes.bool,
};

Day.defaultProps = {
  isActive: false,
  dayClassName: '',
};

export default Day;
