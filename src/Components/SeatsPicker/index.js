import React, { useState, useEffect } from 'react';
import SeatPicker from 'react-seat-picker';
import { Button } from 'antd';
import { useDispatch } from 'react-redux';

const SeatsPicker = ({
  loading, seats, handleSetBooking, addPlaceRequest,
  removePlaceRequest,
}) => {
  const [pickedSeats, setPickedSeats] = useState([]);
  const dispatch = useDispatch();

  const addSeatCallback = ({ row, number, id }, addCb) => {
    console.log(`Added seat ${number}, row ${row}, id ${id}`);
    const newTooltip = `tooltip for id-${id} added by callback`;
    addCb(row, number, id, null);
  };

  const addSeatCallbackContinousCase = ({ row, number, id }, addCb, params, removeCb) => {
    if (removeCb) {
      // console.log(`Removed seat ${params.number}, row ${params.row}, id ${params.id}`);
      // removeCb(params.row, params.number);
      console.log('БАГАТо');
    } else {
      // console.log(`Added seat ${number}, row ${row}, id ${id}`);
      const newTooltip = `tooltip for id-${id} added by callback`;
      setPickedSeats([...pickedSeats, id]);
      addCb(row, number, id, null);
    }
    dispatch(addPlaceRequest({ id, row, number }));
  };

  const removeSeatCallback = ({ row, number, id }, removeCb) => {
    // console.log(`Removed seat ${number}, row ${row}, id ${id}`);
    // A value of null will reset the tooltip to the original while '' will hide the tooltip
    const newTooltip = ['A', 'B', 'C'].includes(row) ? null : '';
    setPickedSeats([...pickedSeats.filter((p) => p !== id)]);
    removeCb(row, number, newTooltip);
    // handleSetBooking(pickedSeats);
    dispatch(removePlaceRequest(id));
  };
console.log(seats)
  return (
    <div>
      <div style={{ marginTop: '100px' }}>
        {
              seats
            && (
            <SeatPicker
              addSeatCallback={addSeatCallbackContinousCase}
              removeSeatCallback={removeSeatCallback}
              rows={seats}
              maxReservableSeats={3}
              alpha
              visible
              selectedByDefault
              loading={loading}
              tooltipProps={{ multiline: true }}
              continuous
            />
            )
            }
      </div>
    </div>
  );
};

export default SeatsPicker;
