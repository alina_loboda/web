import React, { Component } from 'react';

import SeatPicker from 'react-seat-picker';

export default class SeatsPicker extends Component {
    state = {
      loading: false,
    }

    addSeatCallback = ({ row, number, id }, addCb) => {
      this.setState({
        loading: true,
      }, async () => {
        await new Promise((resolve) => setTimeout(resolve, 1500));
        console.log(`Added seat ${number}, row ${row}, id ${id}`);
        const newTooltip = `tooltip for id-${id} added by callback`;
        addCb(row, number, id, newTooltip);
        this.setState({ loading: false });
      });
    }

    addSeatCallbackContinousCase = ({ row, number, id }, addCb, params, removeCb) => {
      this.setState({
        loading: true,
      }, async () => {
        if (removeCb) {
          await new Promise((resolve) => setTimeout(resolve, 750));
          console.log(`Removed seat ${params.number}, row ${params.row}, id ${params.id}`);
          removeCb(params.row, params.number);
        }
        await new Promise((resolve) => setTimeout(resolve, 750));
        console.log(`Added seat ${number}, row ${row}, id ${id}`);
        const newTooltip = `tooltip for id-${id} added by callback`;
        addCb(row, number, id, newTooltip);
        this.setState({ loading: false });
      });
    }

    removeSeatCallback = ({ row, number, id }, removeCb) => {
      this.setState({
        loading: true,
      }, async () => {
        await new Promise((resolve) => setTimeout(resolve, 1500));
        console.log(`Removed seat ${number}, row ${row}, id ${id}`);
        // A value of null will reset the tooltip to the original while '' will hide the tooltip
        const newTooltip = ['A', 'B', 'C'].includes(row) ? null : '';
        removeCb(row, number, newTooltip);
        this.setState({ loading: false });
      });
    }

    render() {
      const rows = [
        [
          { id: 1, number: 1, isReserved: true },
          { id: 2, number: 2 },
          { id: 3, number: 3 },
          { id: 4, number: 4 },
          { id: 5, number: 5 },
          { id: 6, number: 6 },
          { id: 7, number: 7 },
          { id: 8, number: 8 },
          { id: 9, number: 9 },
          { id: 10, number: 10 },
          { id: 11, number: 11 },
          { id: 12, number: 12 },
          { id: 13, number: 13 },
          { id: 14, number: 14 },
          { id: 15, number: 15 },
        ],
        [
          { id: 1, number: 1 },
          { id: 2, number: 2 },
          { id: 3, number: 3 },
          { id: 4, number: 4 },
          { id: 5, number: 5 },
          { id: 6, number: 6 },
          { id: 7, number: 7 },
          { id: 8, number: 8 },
          { id: 9, number: 9 },
          { id: 10, number: 10 },
          { id: 11, number: 11 },
          { id: 12, number: 12 },
          { id: 13, number: 13 },
          { id: 14, number: 14 },
          { id: 15, number: 15 },
        ],
      ];
      const { loading } = this.state;
      console.log(this.props?.seats);
      console.log(rows);

      return (
        <div>
          <div style={{ marginTop: '100px' }}>
            {
              this.props?.seats
            && (
            <SeatPicker
              addSeatCallback={this.addSeatCallbackContinousCase}
              removeSeatCallback={this.removeSeatCallback}
              rows={this.props.seats}
              maxReservableSeats={3}
              alpha
              visible
              selectedByDefault
              loading={loading}
              tooltipProps={{ multiline: true }}
              continuous
            />
)
            }
          </div>
        </div>
      );
    }
}
