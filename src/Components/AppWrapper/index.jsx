import React, { useEffect, useState } from 'react';
import { Layout, Menu } from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  UserOutlined,
} from '@ant-design/icons';
import useSocket from 'use-socket.io-client';
import { useImmer } from 'use-immer';
import { getCurrentUserRequest } from '../../app/rootSlice';

import {
  useHistory,
} from 'react-router-dom';
import { withRouter } from 'react-router';
import './style.scss';
import { useDispatch } from 'react-redux';

const { Footer, Header } = Layout;

const AppWrapper = ({ children, location }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  // const [socket] = useSocket('http://localhost:8001/');
  const [messages, setMessages] = useImmer([]);
  const [online, setOnline] = useImmer([]);
  // socket.connect();
  // console.log(socket);
  //
  // socket.on('on_booking', (data) => {
  //   console.log(data)
  // });

  useEffect(() => {
    if (localStorage.token) {
      dispatch(getCurrentUserRequest());
    }
  });

  return (
    <>
      <Layout style={{ minHeight: '100vh' }} className={location.pathname === '/' && 'home'}>
        <Header>
          <Menu theme="dark" mode="horizontal">
            <Menu.Item key="1" className="menu-item" icon={<PieChartOutlined />} onClick={() => history.push('/')}>
              Головна
            </Menu.Item>
            <Menu.Item key="2" className="menu-item" icon={<DesktopOutlined />} onClick={() => history.push('/archive')}>
              Всі фільми
            </Menu.Item>
            <Menu.Item key="3" className="menu-item" icon={<UserOutlined />} onClick={() => history.push('/profile')}>
              Профіль
            </Menu.Item>
          </Menu>
        </Header>
        <Layout className="site-layout">
          {children}
          <Footer style={{ textAlign: 'center' }}>Footer</Footer>
        </Layout>
      </Layout>
    </>
  );
};

export default withRouter(AppWrapper);
