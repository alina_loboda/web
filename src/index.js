import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
} from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import {StripeProvider} from 'react-stripe-elements';
import App from './App';
import store, { history } from './app/store';
import AppWrapper from './Components/AppWrapper';
import * as serviceWorker from './serviceWorker';
import './index.scss';


ReactDOM.render(
  <>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <BrowserRouter>
          <AppWrapper>
            <App />
          </AppWrapper>
        </BrowserRouter>
      </ConnectedRouter>
    </Provider>
  </>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
