import React from 'react';
import {
  Switch,
} from 'react-router-dom';
import PublicRoute from './Components/Route/PublicRoute';
import PrivateRoute from './Components/Route/PrivateRoute';

import Home from './features/HomePage';
import Profile from './features/Profile';
import SignIn from './features/SignIn';
import SignUp from './features/SignUp';
import MovieDetail from './features/MovieDetail';
import Booking from './features/Booking';
import UpdateProfile from './features/Profile/UpdateProfile';
import Payment from './features/Payment';
import AllMovies from './features/AllMovies';

const AppRouter = () => (
  <Switch>
    <PublicRoute restricted={false} component={Home} path="/" exact className="home-c"/>
    <PublicRoute restricted component={SignIn} path="/signin" exact />
    <PublicRoute restricted component={SignUp} path="/signup" exact />
    <PublicRoute component={MovieDetail} path="/movie/:id" exact />
    <PrivateRoute component={Booking} path="/booking/:id" exact />
    <PrivateRoute component={Profile} path="/profile" exact />
    <PrivateRoute component={UpdateProfile} path="/profile/update" exact />
    <PrivateRoute component={Payment} path="/pay" exact />
    <PrivateRoute component={AllMovies} path="/archive" exact />
  </Switch>
);

export default AppRouter;
