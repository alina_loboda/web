const URIs = {
  development: 'http://localhost:1337',
  fallback: 'http://localhost:1337',
};

export const BASE_API_URL = URIs[process.env.REACT_APP_NODE_ENV] || URIs.fallback;

export const CONTENT_TYPES = {
  JSON: 'application/json',
  MULTIPART_DATA: 'multipart/form-data',
};

export const isFile = (file) => file instanceof File;
export const isObject = (obj) => obj instanceof Object;
