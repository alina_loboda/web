import axios from 'axios';
import queryString from 'qs';
import {
  BASE_API_URL, CONTENT_TYPES, isFile, isObject,
} from './Constants';

function createFormWithData(data, parentKey, formData = new FormData()) {
  Object.keys(data).forEach((key) => {
    const val = data[key];

    if (parentKey) {
      key = `${parentKey}[${key}]`;
    }

    if (isObject(val) && !isFile(val) && !Array.isArray(val)) {
      return createFormWithData(val, key, formData);
    }

    if (Array.isArray(val)) {
      val.forEach((v, idx) => {
        if (isObject(v)) {
          createFormWithData(v, `${key}[${idx}]`, formData);
        } else {
          formData.append(`${key}[${idx}]`, v);
        }
      });
    } else {
      formData.append(key, val);
    }
  });

  return formData;
}

const getBody = (contentType, data) => {
  switch (contentType) {
    case CONTENT_TYPES.JSON:
      return data;
    case CONTENT_TYPES.MULTIPART_DATA:
      return createFormWithData(data);
    default:
      return data;
  }
};

export function apiRequest(
  url = '',
  method = 'GET',
  payload = undefined,
  authorize = true,
  contentType = CONTENT_TYPES.JSON,
) {
  let headers = {
    'Content-Type': contentType,
    Accept: 'application/json',
  };

  if (authorize && localStorage.token) {
    headers = { ...headers, Authorization: `Bearer ${localStorage.token}` };
  }

  return axios({
    method,
    params: (method === 'GET' ? payload : {}),
    data: (method === 'GET' ? {} : getBody(contentType, payload)),
    crossDomain: true,
    headers,
    url: BASE_API_URL + url,
    paramsSerializer: (params) => queryString.stringify(params, { arrayFormat: 'brackets' }),
  });
  /* .catch(error => {
      throw new SubmissionError(error.response); }); */
}
