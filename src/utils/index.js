import {BASE_API_URL} from "./Constants";

const TOKEN_KEY = 'token';

export const login = (token) => {
  localStorage.setItem(TOKEN_KEY, token);
};

export const logout = () => {
  localStorage.removeItem(TOKEN_KEY);
};

export const isLogin = () => {
  if (localStorage.getItem(TOKEN_KEY)) {
    return true;
  }

  return false;
};

export const minutesToHours = (m) => {
  const hours = (m / 60);
  const rhours = Math.floor(hours);
  const minutes = (hours - rhours) * 60;
  const rminutes = Math.round(minutes);
  return `${rhours}:${rminutes}`;
};

export const renderGenres = (genres, sign) => (
  genres.map((g, i) => (
    `${g.name} ${genres.length !== i + 1 ? sign : ''}`
  ))
);

export const getAvatarFullUrl = (url)=> {
  return url ? BASE_API_URL+url : 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png';
}
