import { createSlice } from '@reduxjs/toolkit';
import { message } from 'antd';
import { apiRequest } from '../../utils/Api';

export const movieDetailSlice = createSlice({
  name: 'movieDetail',
  initialState: {
    data: [],
    comments: [],
    starRating: null,
    loading: false,
    commentsLoading: false,
    createCommentLoading: false,
  },
  reducers: {
    getMovie: (state, action) => {
      state.data = action.payload;
    },
    getMovieLoading: (state, action) => {
      state.loading = action.payload;
    },
    getComments: (state, action) => {
      state.comments = action.payload;
    },
    getCommentsLoading: (state, action) => {
      state.commentsLoading = action.payload;
    },
    createComment: (state, action) => {
      state.comments = [...state.comments, ...[action.payload]];
    },
    createCommentLoading: (state, action) => {
      state.createCommentLoading = action.payload;
    },
    getStarRating: (state, action) => {
      state.starRating = action.payload;
    },
    setStarRating: (state, action) => {
      state.starRating = action.payload;
    },
    updateStarRating: (state, action) => {
      state.starRating = action.payload;
    },
  },
});

export const {
  getMovie,
  getMovieLoading,
  getComments,
  getCommentsLoading,
  createComment,
  createCommentLoading,
  getStarRating,
  setStarRating,
  updateStarRating,
} = movieDetailSlice.actions;

export const getMovieRequest = (id) => (dispatch) => {
  dispatch(getMovieLoading(true));
  apiRequest(`/movies/${id}`, 'GET')
    .then((response) => {
      dispatch(getMovieLoading(false));
      dispatch(getMovie(response.data));
    }).catch((err) => {
      message.error('Щось пішло не так (не можу взяти фільм)');
      dispatch(getMovieLoading(false));
    });
};

export const getCommentsRequest = (id) => (dispatch) => {
  dispatch(getCommentsLoading(true));
  apiRequest(`/comments/?movie.id=${id}`, 'GET')
    .then((response) => {
      dispatch(getCommentsLoading(false));
      dispatch(getComments(response.data));
    }).catch((err) => {
      message.error('Щось пішло не так (не можу взяти коменти)');
      dispatch(getCommentsLoading(false));
    });
};

export const createCommentsRequest = (id, userId, content) => (dispatch) => {
  const params = {
    movie: id,
    user: userId,
    body: content,
  };
  dispatch(createCommentLoading(true));
  apiRequest('/comments', 'POST', params)
    .then((response) => {
      dispatch(createCommentLoading(false));
      dispatch(createComment(response.data));
    }).catch((err) => {
      message.error('Щось пішло не так (не можу додати коменти)');
      dispatch(createCommentLoading(false));
    });
};

export const getStarRatingRequest = (movieId, userId) => (dispatch) => {
  apiRequest(`/star-rating/?movie.id=${movieId}&user.id=${userId}`, 'GET')
      .then((response) => {
        dispatch(getStarRating(response.data))
      }).catch((err) => {
    message.error('Щось пішло не так (не можу взяти коменти)');
  });
};

export const movie = (state) => state.movieDetail.data;
export const movieLoading = (state) => state.movieDetail.loading;
export const commentsData = (state) => state.movieDetail.comments;
export const commentsDataLoading = (state) => state.movieDetail.commentsLoading;
export const createCommentRequestLoading = (state) => state.movieDetail.createCommentLoading;

export default movieDetailSlice.reducer;
