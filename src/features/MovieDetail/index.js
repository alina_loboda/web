import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Layout, Typography, Button, Divider, Rate,
} from 'antd';
import propTypes from 'prop-types';
import moment from 'moment';
import 'moment/locale/uk';
import { CaretRightOutlined } from '@ant-design/icons';
import CommentListView from '../../Components/CommentsList';
import {
  movie,
  movieLoading,
  getMovieRequest,
  getCommentsRequest,
  createCommentsRequest,
  commentsData,
  commentsDataLoading,
  createCommentRequestLoading,
  getStarRatingRequest,
} from './movieDetailSlice';
import { getAvatarFullUrl, minutesToHours, renderGenres } from '../../utils';
import './style.scss';

const { Content } = Layout;
const { Title, Text } = Typography;

const MovieDetail = ({ history, match, location }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.root?.currentUser);

  const data = useSelector(movie);
  const loading = useSelector(movieLoading);

  const comments = useSelector(commentsData);
  const commentsLoading = useSelector(commentsDataLoading);
  const createCommentLoading = useSelector(createCommentRequestLoading);

  useEffect(() => {
    dispatch(getMovieRequest(match.params.id));
    dispatch(getCommentsRequest(match.params.id));
    // dispatch(getStarRatingRequest(match.params.id));
  }, [dispatch, match.params.id]);

  const handleAddComment = (body, form) => {
    dispatch(createCommentsRequest(match.params.id, currentUser.id, body));
    form.resetFields();
  };

  const renderMovieInfoRow = (name, text) => (
    <Text>
      <Text strong>
        {' '}
        {`${name}: `}
      </Text>
      {text}
    </Text>
  );

  const handleStartFilm = (mark) =>{
    console.log(mark)
  }

  return (
    <Content className="movie-detail">
      <div
        className="top-section"
        style={{
          backgroundImage: `url(${getAvatarFullUrl(data?.widePoster?.url)})`,
        }}
      >
        <div className="movie-detail-header">
          <Title className="movie-detail-header-title">{data?.title}</Title>
          <div className="movie-detail-header-extra">
            {minutesToHours(data.duration)}
            {' '}
            |
            {' '}
            {data?.genres && renderGenres(data?.genres, ', ')}
          </div>
          <div className="watch-now">
            <Button
              danger
              size="large"
              type="primary"
              className="watch-now-btn"
            >
              Дивитися трейлер
              <CaretRightOutlined />
            </Button>
          </div>
        </div>
      </div>
      <div className="movie-content">
        <div className="movie-content-header">
          <div className="header-rating">
            <span>{data.IMDB}</span>
            <span>/ 10</span>
          </div>
          <div className="date">
            {
              moment(data.releaseDate).format('DD MMMM YYYY')
            }
          </div>
          <Button ghost href={`/booking/${match.params.id}`}>Замовити квитки</Button>

        </div>
        <Content className="main-detail-container">
          <div className="movie-cover">
            <img
              src={getAvatarFullUrl(data?.poster?.url)}
              className="movie-cover-pic"
            />
            <Rate onChange={handleStartFilm} allowClear={false} value={3}/>
          </div>
          <div className="info-block">
            {renderMovieInfoRow('Вік', data.ageRating)}
            {renderMovieInfoRow('Оригінальна назва', data.originalTitle)}
            {renderMovieInfoRow('Режисер', data.Directors)}
            {renderMovieInfoRow('Дата виходу', moment(data.releaseDate).format('DD.MM.YYYY'))}
            {renderMovieInfoRow('Жанр', data.genres && renderGenres(data.genres, ', '))}
            {renderMovieInfoRow('Тривалість', minutesToHours(data.duration))}
            {renderMovieInfoRow('Виробництво', data.country)}
            {renderMovieInfoRow('Сценарій', data.screenwriters)}
            {renderMovieInfoRow('У головних ролях', data.Cast)}
            <p />
            {
              data.description
            }
          </div>
        </Content>
        <Divider />
        <div className="comment-list">
          <CommentListView
            comments={comments}
            loading={createCommentLoading}
            addComment={handleAddComment}
          />

        </div>
      </div>
    </Content>
  );
};

MovieDetail.propTypes = {
  data: propTypes.shape({
    Cast: propTypes.string.isRequired,
    Directors: propTypes.string.isRequired,
    IMDB: propTypes.number,
    ageRating: propTypes.string.isRequired,
    country: propTypes.string.isRequired,
    createdAt: propTypes.string.isRequired,
    description: propTypes.string.isRequired,
    duration: propTypes.number.isRequired,
    genres: propTypes.array.isRequired,
    id: propTypes.string.isRequired,
    originalTitle: propTypes.string.isRequired,
    poster: propTypes.object,
    releaseDate: propTypes.string,
    screenwriters: propTypes.string.isRequired,
    startShowDate: propTypes.string,
    title: propTypes.string.isRequired,
  }).isRequired,
  loading: propTypes.bool.isRequired,
};

export default MovieDetail;
