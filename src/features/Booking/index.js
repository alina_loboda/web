import React, { useState, useEffect } from 'react';
import {
  Layout, Button, Row, Col, PageHeader,
} from 'antd';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import Day from '../../Components/Day';
import Ticket from '../../Components/Ticket';
import SeatsPicker from '../../Components/SeatsPicker';
import {
  getSeances,
  seancesDate,
  getBookedSeatsRequest,
  bookedSeatsData,
  bookedSeatsLoadingData,
  createBookingRequest,
  bookedTicketsData,
  selectedPlacesData,
  addPlaceRequest,
  removePlaceRequest,
  clearPlaceRequest,
  setInitialBookingRequest,
} from './bookingSlice';
import { getMovieRequest, movie } from '../MovieDetail/movieDetailSlice';
import { getAvatarFullUrl } from '../../utils';
import './style.scss';

const { Content } = Layout;

const Booking = ({ match, history }) => {
  const [activeDate, setActiveDate] = useState('');
  const [activeTime, setActiveTime] = useState('');
  const [helperText, setHelperText] = useState(' дату');
  const dispatch = useDispatch();
  const seances = useSelector(seancesDate);
  const bookedSeats = useSelector(bookedSeatsData);
  const bookedSeatsLoading = useSelector(bookedSeatsLoadingData);
  const bookedTickets = useSelector(bookedTicketsData);
  const userId = useSelector((state) => state.root.currentUser.id);
  const movieInfo = useSelector(movie);
  const selectedPlaces = useSelector(selectedPlacesData);

  useEffect(() => {
    dispatch(getMovieRequest(match.params.id));
    dispatch(getSeances(match.params.id));
  }, [dispatch, match.params.id]);

  const handleSetBooking = () => {
    const hallId = seances.data.find((s) => s.id === activeTime);
    const placeIds = selectedPlaces.map((p) => p.id);

    // dispatch(createBookingRequest(activeTime, placeIds, hallId.hall.id, userId));
    // console.log(placeIds);
  };

  const handlePay = () => {
    const d = selectedPlaces.map((p) => p.id);
    history.push({
      pathname: '/pay',
      state: {
        hallId: seances.data.find((s) => s.id === activeTime),
        placeIds: d,
        userId,
        activeTime,
      },
    });
  };

  const toggleActive = (d) => {
    setActiveDate(d);
    setActiveTime('');
    setHelperText(' час');
    dispatch(clearPlaceRequest());
    dispatch(setInitialBookingRequest());
  };

  const toggleTime = (s) => {
    setActiveTime(s);
    dispatch(getBookedSeatsRequest(s));
    setHelperText(' місця');
    dispatch(clearPlaceRequest());
    // dispatch(setInitialBookingRequest());
  };
  let groupArrays = [];

  if (seances?.data) {
    const groups = seances.data.reduce((groups, game) => {
      const date = game.seanceStart.split('T')[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(game);
      return groups;
    }, {});

    // Edit: to add it in the array format instead
    groupArrays = Object.keys(groups).map((date) => ({
      date,
      seances: groups[date],
    }));
  }

  return (
    <>
      <PageHeader
        className="site-page-header"
        onBack={() => history.push(`/movie/${match.params.id}`)}
        title={movieInfo?.title}
      />

      <Content>
        <Row justify="center" align="top">
          <Col span={16}>
            <img src={getAvatarFullUrl(movieInfo?.widePoster?.url)} className="booking-top-section" />
            <h2 className="text-center">
              Виберіть
              {helperText}
            </h2>
            <div className="days-list">
              {
          groupArrays.map((d, i) => (
            <Day
              date={d.date}
              key={i}
              handleActive={() => toggleActive(d.date)}
              isActive={d.date === activeDate}
            />
          ))
        }
            </div>
            <div className="seances-list">
              {activeDate
        && groupArrays.filter((g) => g.date === activeDate).map((s) => (
          s.seances.map((seance) => (
            <Button
              key={`${seance.id}-s`}
              className="seance"
              size="large"
              onClick={() => toggleTime(seance.id)}
              type={activeTime === seance.id ? 'primary' : 'dashed'}
            >
              {moment(seance.seanceStart).format('hh:mm')}
            </Button>
          ))
        ))}
            </div>
            {
              <SeatsPicker
                seats={bookedSeats}
                loading={bookedSeatsLoading}
                addPlaceRequest={addPlaceRequest}
                removePlaceRequest={removePlaceRequest}
                selectedPlaces={selectedPlaces}
              />
      }
            {
        bookedTickets.length > 0 && bookedTickets.map((t) => (
          <div key={t.id}>
            <p>
              <img src={t.qrCode} />
              {' '}
              test
              {' '}
            </p>
          </div>
        ))
      }
          </Col>
          <Col span={8} className="ticket-place">
            {
            movieInfo?.poster
          && (
          <Ticket
            movie={movieInfo}
            seance={activeTime && seances.data.find((s) => s.id === activeTime)}
            selectedPlaces={selectedPlaces}
            handlePay={handlePay}
          />
)
          }
          </Col>
        </Row>
      </Content>
    </>
  );
};

export default Booking;
