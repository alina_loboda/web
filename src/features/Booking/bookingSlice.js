import { createSlice } from '@reduxjs/toolkit';
import { message } from 'antd';
import { apiRequest } from '../../utils/Api';

const cols = [...Array(15).keys()];
const rows = [...Array(10).keys()];

const initialCols = cols.map((c, i) => (
  { id: i + 1, number: i + 1, isReserved: true }
));

const initialPlaces = rows.map((r) => (
  initialCols
));

export const bookingSlice = createSlice({
  name: 'booking',
  initialState: {
    seances: {
      data: [],
      loading: false,
    },
    bookedSeats: initialPlaces,
    bookedSeatsLoading: false,
    bookedTickets: [],
    bookedTicketsLoading: false,
    selectedPlaces: [],
  },
  reducers: {
    setSeances: (state, action) => (
      {
        ...state,
        seances: {
          data: action.payload,
          loading: false,
        },
      }
    ),
    setSeancesLoading: (state, action) => {
      state.seances.loading = action.payload;
    },

    getBookedSeats: (state, action) => {
      state.bookedSeats = action.payload;
    },
    setBookedSeatsLoading: (state, action) => {
      state.bookedSeatsLoading = action.payload;
    },
    setBookedTickets: (state, action) => {
      state.bookedTickets = action.payload;
    },
    setBookedTicketsLoading: (state, action) => {
      state.bookedTicketsLoading = action.payload;
    },
    addPlace: (state, action) => {
      state.selectedPlaces = [...state.selectedPlaces, action.payload];
    },
    removePlace: (state, action) => {
      state.selectedPlaces = state.selectedPlaces.filter((s) => s.id !== action.payload);
    },
    clearPlace: (state, action) => {
      state.selectedPlaces = [];
    },
    setInitialBooking: (state, action) => {
      state.bookedSeats = initialPlaces;
    },
  },
});

export const {
  setSeancesLoading,
  setSeances,
  getBookedSeats,
  setBookedSeatsLoading,
  setBookedTickets,
  setBookedTicketsLoading,
  addPlace,
  removePlace,
  clearPlace,
  setInitialBooking,
} = bookingSlice.actions;


export const getSeances = (movieId) => (dispatch) => {
  dispatch(setSeancesLoading(true));
  apiRequest(`/seances?movie.id=${movieId}`, 'GET').then((response) => {
    dispatch(setSeancesLoading(false));
    dispatch(setSeances(response.data));
  }).catch((error) => {
    message.error('Некоретно введені дані.');
    dispatch(setSeancesLoading(false));
  });
};

export const getBookedSeatsRequest = (seanceId) => (dispatch) => {
  dispatch(setBookedSeatsLoading(true));
  apiRequest(`/cinema-seances/${seanceId}`, 'GET').then((response) => {
    const groups = response.data.booked_places.reduce((groups, game) => {
      const date = game.row;
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push({
        id: game.id, number: game.col, isReserved: game.booked,
      });
      return groups;
    }, {});

    dispatch(getBookedSeats(Object.values(groups)));
    dispatch(setBookedSeatsLoading(false));
  }).catch((error) => {
    message.error('Неможу взяти букінг');
    dispatch(setBookedSeatsLoading(false));
  });
};

export const createBookingRequest = (seances, places, hall, userId) => (dispatch) => {
  const data = {
    seances: [seances],
    places,
    hall,
    user: userId,
    isConfirmed: true,
  };
  const tikets = [];

  dispatch(setBookedTicketsLoading(true));
  apiRequest('/bookings', 'POST', data)
    .then((response) => {
      dispatch(setBookedTicketsLoading(false));
      dispatch(setBookedTickets(Object.values(response.data)));
      setTimeout(()=>{
        message.success('Успішно оплачено');
      }, 3000)
    }).catch((error) => {
      console.log(error);
      message.error('Некоретно введені дані.');
      dispatch(setBookedTicketsLoading(false));
    });
};

export const addPlaceRequest = (place) => (dispatch) => {
  dispatch(addPlace(place));
};

export const removePlaceRequest = (place) => (dispatch) => {
  dispatch(removePlace(place));
};
export const clearPlaceRequest = () => (dispatch) => {
  dispatch(clearPlace());
};
export const setInitialBookingRequest = () => (dispatch) => {
  dispatch(setInitialBooking());
};


export const selectCount = (state) => state.counter.value;
export const seancesDate = (state) => state.booking.seances;
export const bookedSeatsData = (state) => state.booking.bookedSeats;
export const bookedSeatsLoadingData = (state) => state.booking.bookedSeatsLoading;

export const bookedTicketsData = (state) => state.booking.bookedTickets;
export const bookedTicketsLoadingData = (state) => state.booking.bookedTicketsLoading;
export const selectedPlacesData = (state) => state.booking.selectedPlaces;

export default bookingSlice.reducer;
