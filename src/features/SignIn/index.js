import React, { useEffect } from 'react';
import {
  Form, Input, Layout, Button, Checkbox, Typography,
} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import {
  selectIsLogedIn,
  loginRequest,
} from '../../app/rootSlice';
import './style.scss';
import { isLogin } from '../../utils';

const { Content } = Layout;
const { Title } = Typography;

const SignIn = ({ history }) => {
  useEffect(() => {
    if (isLogin()) {
      history.push('/profile');
    }
  });

  const isLogedIn = useSelector(selectIsLogedIn);
  const dispatch = useDispatch();

  const onFinish = (values) => {
    dispatch(loginRequest(values));
    console.log('Received values of form: ', values);
  };

  return (
    <Content style={{ margin: '0 16px' }}>
      <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
        <div className="App">
          <Form
            name="normal_login"
            className="auth login-form"
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Title level={3}> Вхід </Title>
            <Form.Item
              name="accUsername"
              rules={[{ required: true, message: 'Please input your Username!' }]}
            >
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Нікнейм" />
            </Form.Item>
            <Form.Item
              name="accPassword"
              rules={[{ required: true, message: 'Please input your Password!' }]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Пароль"
              />
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button" loading={isLogedIn}>
                Увійти
              </Button>
              або
              {' '}
              <a href="/signup">зареєструватися</a>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Content>
  );
};

export default SignIn;
