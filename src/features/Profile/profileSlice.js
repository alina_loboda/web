import { createSlice } from '@reduxjs/toolkit';
import { message } from 'antd';
import moment from 'moment';
import { apiRequest } from '../../utils/Api';

export const movieDetailSlice = createSlice({
  name: 'profile',
  initialState: {
    active: [],
    activeLoad: false,
  },
  reducers: {
    getActiveBooking: (state, action) => {
      state.active = action.payload;
    },
    getActiveBookingLoading: (state, action) => {
      state.activeLoad = action.payload;
    },
  },
});

export const {
  getActiveBooking,
  getActiveBookingLoading,
} = movieDetailSlice.actions;

export const getActiveBookingRequest = (userId, status) => (dispatch) => {
  dispatch(getActiveBookingLoading(true));
  apiRequest(`/bookingsa?seances.seanceStart_${status === 'active' ? 'g' : 'l'}te=${moment().format()}&user.id=${userId}`, 'GET')
    .then((response) => {
      dispatch(getActiveBookingLoading(false));
      dispatch(getActiveBooking(response.data));
    }).catch((err) => {
      message.error('Щось пішло не так (не можу взяти фільм)');
      dispatch(getActiveBookingLoading(false));
    });
};

export const myBookings = (state) => state.profile.active;
export const myBookingsLoading = (state) => state.profile.activeLoad;

export default movieDetailSlice.reducer;
