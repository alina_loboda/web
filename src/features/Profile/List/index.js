import React from 'react';
import {
  List, Avatar, Typography, Skeleton,
} from 'antd';
import {getAvatarFullUrl} from "../../../utils";
import moment from "moment";
const { Paragraph } = Typography;

const TicketList = ({data}) => (
  <List
    className="demo-loadmore-list"
    itemLayout="horizontal"
    dataSource={data}
    renderItem={(item) => (
      <List.Item
      >
        <Skeleton avatar title={false} loading={item.loading} active>
          <List.Item.Meta
            avatar={
                <img width={100} src={getAvatarFullUrl(item.seanceData[0].movie?.poster?.url)}/>
            }
            title={<a href="https://ant.design">{item.seanceData[0].movie.title}</a>}
            description={item?.places.length > 0
                && (
                <div>
                    <p style={{marginBottom: '0.3em'}}>Зал: {item.seanceData[0].hall.name}</p>
                    <p style={{marginBottom: '0.3em'}}>Час: {moment(item.seanceData[0].seanceStart).format('hh:mm DD/MM/YYYY')}</p>
                    <p style={{marginBottom: '0.3em'}}>Ряд: {item?.places[0].row}</p>
                    <p>Місце: {item?.places[0].col}</p>
                    <p></p>
                    <a key="list-loadmore-more">Завантажити</a>
                </div>
            )
            }
          />
            <img width={100} src={item.qrCode}/>

        </Skeleton>
      </List.Item>
    )}
  />
);

export default TicketList;
