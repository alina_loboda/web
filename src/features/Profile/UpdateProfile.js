import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Avatar, Row, Col, Layout, Card, Typography,
  Form, Input, Button, Radio, Upload,
} from 'antd';

import { UploadOutlined } from '@ant-design/icons';

import { BASE_API_URL } from '../../utils/Constants';

import {
  selectCurrentUserInfo, updateProfileRequest, selectUpdateProfileLoading, uploadPic,
} from '../../app/rootSlice';

import './style.scss';

const { Text } = Typography;

const { Content } = Layout;

const tabListNoTitle = [
  {
    key: 'article',
    tab: 'article',
  },
  {
    key: 'app',
    tab: 'app',
  },
  {
    key: 'project',
    tab: 'project',
  },
];

const contentListNoTitle = {
  article: <p>article content</p>,
  app: <p>app content</p>,
  project: <p>project content</p>,
};

const UpdateProfile = ({ history }) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [fileList, setFileList] = useState([]);
  const [uploading, setUploading] = useState(false);


  const currentUserInfo = useSelector(selectCurrentUserInfo);
  const updateProfileLoading = useSelector(selectUpdateProfileLoading);

  const renderAvatar = () => {
    const avatarUrl = `${BASE_API_URL}${currentUserInfo?.avatar?.url}`;
    return (
      <Avatar size={100} src={currentUserInfo?.avatar?.url ? avatarUrl : 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png'} />
    );
  };

  const onFinish = (values) => {
    dispatch(updateProfileRequest(currentUserInfo.id, values, history));
  };

  const initialValues = {
    firstName: currentUserInfo?.firstName,
    lastName: currentUserInfo?.lastName,
    email: currentUserInfo?.email,
    username: currentUserInfo?.username,
  };

  const props = {
    multiple: false,
    showUploadList: {
      showDownloadIcon: false,
    },
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList.slice(-1), file]);
      return false;
    },
    fileList,
  };

  const handleUpload = () => {
    const formData = new FormData();
    fileList.forEach((file) => {
      formData.append('avatar', file);
      console.log(file);
    });
    dispatch(uploadPic(currentUserInfo.id, { files: fileList[0] }, history));
    setUploading(true);
  };

  return (
    <Content className="profile-container">
      <Row>
        <Col span={6} className="main-col">
          <Card>
            <div className="avatar-holder">
              {renderAvatar()}
              <p />
              <Upload name="avatar" {...props}>
                <Button>
                  <UploadOutlined />
                  {' '}
                  Натисність для вибору файла
                </Button>
              </Upload>
              {
                  fileList.length !== 0
                && (
                <Button
                  type="primary"
                  onClick={handleUpload}
                  disabled={fileList.length === 0}
                  loading={uploading}
                  style={{ marginTop: 16 }}
                >
                  {uploading ? 'Завантаження' : 'Завантажити'}
                </Button>
                )
                }
            </div>
          </Card>
        </Col>
        <Col span={18} className="main-col">
          <Card>
            {currentUserInfo?.firstName
              && (
              <Form
                layout="vertical"
                form={form}
                name="profile"
                initialValues={initialValues}
                onFinish={onFinish}
              >

                <Form.Item name="firstName" label="І'мя">
                  <Input placeholder="" />
                </Form.Item>
                <Form.Item name="lastName" label="Прізвище">
                  <Input placeholder="" />
                </Form.Item>
                <Form.Item name="username" label="Логін">
                  <Input placeholder="" />
                </Form.Item>
                <Form.Item name="email" label="Email">
                  <Input placeholder="" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit" loading={updateProfileLoading}>Оновити</Button>
                </Form.Item>
              </Form>
              )}
          </Card>
        </Col>
      </Row>
    </Content>
  );
};

export default UpdateProfile;
