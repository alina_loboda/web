import React from 'react';
import { Card } from 'antd';
import './import.scss';

const TicketItem = ({ movie }) => (
  <Card type="inner" title="Inner Card title" extra={<a href="#">More</a>}>
    Inner Card content
  </Card>
);

export default TicketItem;
