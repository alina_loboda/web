import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Avatar, Row, Col, Layout, Card, Typography, Button, Empty, Spin, Space,
} from 'antd';
import { BASE_API_URL } from '../../utils/Constants';
import { selectCurrentUserInfo } from '../../app/rootSlice';
import { getActiveBookingRequest, myBookings, myBookingsLoading } from './profileSlice';
import TicketItem from './Item';
import TicketList from "./List";
import './style.scss';

const { Text } = Typography;
const { Content } = Layout;

const tabListNoTitle = [
  {
    key: 'active',
    tab: 'Активні квитки',
  },
  {
    key: 'archive',
    tab: 'Архів',
  },
];

const contentListNoTitle = {
  article: <p>article content</p>,
  app: <p>app content</p>,
  project: <p>project content</p>,
};

const Profile = () => {
  const [noTitleKey, setNoTitleKey] = useState('active');
  const dispatch = useDispatch();
  const currentUserInfo = useSelector(selectCurrentUserInfo);
  const myBookingsData = useSelector(myBookings);
  const myBookingsLoadingData = useSelector(myBookingsLoading);


  useEffect(() => {
    currentUserInfo?.id && dispatch(getActiveBookingRequest(currentUserInfo.id, 'active'));
  }, [currentUserInfo, currentUserInfo.id, dispatch]);

  const onTabChange = (key) => {
    setNoTitleKey(key);
    if (key === 'active') {
      dispatch(getActiveBookingRequest(currentUserInfo.id, 'active'));
    } else {
      dispatch(getActiveBookingRequest(currentUserInfo.id));
    }
  };

  const renderAvatar = () => {
    const avatarUrl = `${BASE_API_URL}${currentUserInfo?.avatar?.url}`;
    return (
      <Avatar size={100} src={currentUserInfo?.avatar?.url ? avatarUrl : 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png'} />
    );
  };

console.log(myBookingsData)
  return (
    <Content className="profile-container">
      <Row>
        <Col span={6} className="main-col">
          <Card>
            <div className="avatar-holder">
              {renderAvatar()}
              <div className="antd-pro-pages-account-center-center-name">
                {`${currentUserInfo?.firstName || ''} ${currentUserInfo?.lastName || ''}`}
              </div>
              <div>
                <Text type="secondary">Ім'я користувача: </Text>
                {' '}
                {currentUserInfo?.username}
              </div>
              <div>
                <Text type="secondary">Email: </Text>
                {' '}
                {currentUserInfo?.email}
              </div>
            </div>
            <Button block href="/profile/update"> Редагувати профіль</Button>
          </Card>
        </Col>
        <Col span={18} className="main-col">
          <Card
            style={{ width: '100%' }}
            tabList={tabListNoTitle}
            activeTabKey={noTitleKey}
            onTabChange={(key) => {
              onTabChange(key);
            }}
          >
            {!myBookingsLoadingData && myBookingsData.length === 0
            && (
            <Empty
              description={(
                <span>
                  Квиточки відсутні
                </span>
                )}
            />
            )}
            {/*{myBookingsLoadingData && (*/}
            {/*<Space size="middle">*/}
            {/*  <Spin size="large" />*/}
            {/*</Space>*/}
            {/*)}*/}
            {
              <TicketList data={myBookingsData} />
            }
            {/*<TicketItem />*/}
          </Card>

        </Col>
      </Row>
    </Content>
  );
};

export default Profile;
