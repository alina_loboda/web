import React, { useState, useEffect } from 'react';
import {
  Form, Input, Button, Layout, DatePicker, Select,
} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

import './styles.scss';

const { Option } = Select;
const { RangePicker } = DatePicker;
const { Content } = Layout;
const AllMovies = () => {
  const [form] = Form.useForm();
  const [, forceUpdate] = useState();

  // To disable submit button at the beginning.
  useEffect(() => {
    forceUpdate({});
  }, []);

  const onFinish = (values) => {
    console.log('Finish:', values);
  };
  return (
    <Content style={{ margin: '16px' }}>
      <Form form={form} name="horizontal_login" layout="inline" onFinish={onFinish}>
        <Form.Item name="range-picker" label="Дата">
          <RangePicker />
        </Form.Item>
        <Form.Item label="Жанр" name="genres">
          <Select mode="multiple" placeholder="" className="multiple-genre-select">
            <Option value="red">Red</Option>
            <Option value="green">Green</Option>
            <Option value="blue">Blue</Option>
          </Select>
        </Form.Item>
      </Form>
    </Content>
  );
};

export default AllMovies;
