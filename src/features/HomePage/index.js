import React, { useEffect } from 'react';
import moment from 'moment';
import {
  Col, Layout, Row, Typography, Skeleton,Tabs,
} from 'antd';
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import {
  getMoviesByGenres, getMovies, movies, moviesLoading,
  moviesByGenres,
  moviesByGenresLoading,
} from './counterSlice';
import MovieCard from '../../Components/Card';
import './style.scss';
import { useDispatch, useSelector } from 'react-redux';

const { Content } = Layout;
const { TabPane } = Tabs;
const { Title } = Typography;

const Home = ({history}) => {
  const dispatch = useDispatch();
  const allMovies = useSelector(movies);
  const allMoviesLoading = useSelector(moviesLoading);

  const moviesByGenresData = useSelector(moviesByGenres);
  const moviesByGenresDataLoading = useSelector(moviesByGenresLoading);

  useEffect(() => {
    dispatch(getMovies());
    dispatch(getMoviesByGenres('Комедія'));
  }, [dispatch]);

const p = [1, 2, 3].map((e) => (
    <Skeleton key={e} active loading paragraph={{ rows: 6 }} style={{maxWidth: 350}}/>
));

const handleTabChange = (key)=>{
  dispatch(getMoviesByGenres(key));

console.log(key)
}

  return (
    <Content style={{ margin: '0 16px' }}>
      <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
        <div className="App home-content">
          <Title level={2}>Зараз в прокаті</Title>

          <Carousel
            clickToChange
            slidesPerPage={3}
            infinite
            slides={allMoviesLoading && p}
          >
            {!allMoviesLoading && allMovies.map((m) => (
              <MovieCard
                src={m.poster.url}
                key={m.id}
                id={m.id}
                title={m.title}
                genres={m.genres.slice(-3) || []}
                rating={m.IMDB}
                year={moment(m.releaseDate).format('YYYY')}
                duration={m.duration}
                ageRating={m.ageRating}
                loading={allMoviesLoading}
                showHeaderExtra
                heightLimited
                hadleClick={()=>{history.push(`/movie/${m.id}`)}}
              />
            ))}
          </Carousel>


          <Title level={2}>Скоро в кіно</Title>
          <Tabs defaultActiveKey="Комедія" onChange={handleTabChange}>
            <TabPane tab="Комедія" key="Комедія">
              <Carousel
                  clickToChange
                  slidesPerPage={3}
                  infinite
                  slides={moviesByGenresDataLoading && p}
              >
                {!moviesByGenresDataLoading && moviesByGenresData.map((m) => (
                    <MovieCard
                        src={m.widePoster.url}
                        key={m.id}
                        title={m.title}
                        genres={m.genres.slice(-3) || []}
                        rating={m.IMDB}
                        year={moment(m.releaseDate).format('YYYY')}
                        duration={m.duration}
                        ageRating={m.ageRating}
                        loading={moviesByGenresDataLoading}
                        showHeaderExtra
                        wide
                    />
                ))}
              </Carousel>
            </TabPane>
            <TabPane tab="Пригоди" key="Пригоди">
              <Carousel
                  clickToChange
                  slidesPerPage={3}
                  infinite
                  slides={moviesByGenresDataLoading && p}
              >
                {!moviesByGenresDataLoading && moviesByGenresData.map((m) => (
                    <MovieCard
                        src={m.widePoster.url}
                        key={m.id}
                        title={m.title}
                        genres={m.genres.slice(-3) || []}
                        rating={m.IMDB}
                        year={moment(m.releaseDate).format('YYYY')}
                        duration={m.duration}
                        ageRating={m.ageRating}
                        loading={moviesByGenresDataLoading}
                        showHeaderExtra
                        wide
                    />
                ))}
              </Carousel>
            </TabPane>
            <TabPane tab="Драма" key="Драма">
              <Carousel
                  clickToChange
                  slidesPerPage={3}
                  infinite
                  slides={moviesByGenresDataLoading && p}
              >
                {!moviesByGenresDataLoading && moviesByGenresData.map((m) => (
                    <MovieCard
                        src={m.widePoster.url}
                        key={m.id}
                        title={m.title}
                        genres={m.genres.slice(-3) || []}
                        rating={m.IMDB}
                        year={moment(m.releaseDate).format('YYYY')}
                        duration={m.duration}
                        ageRating={m.ageRating}
                        loading={moviesByGenresDataLoading}
                        showHeaderExtra
                        wide
                    />
                ))}
              </Carousel>
            </TabPane>
          </Tabs>
        </div>
      </div>
    </Content>
  );
};

export default Home;
