import { createSlice } from '@reduxjs/toolkit';
import { message } from 'antd';
import { apiRequest } from '../../utils/Api';

export const counterSlice = createSlice({
  name: 'homePage',
  initialState: {
    movies: [],
    moviesLoading: false,
    moviesByGenres: [],
    moviesByGenresLoading: false,
  },
  reducers: {
    setMoviesByGenres: (state, action) => {
      state.moviesByGenres = action.payload;
    },
    setMoviesByGenresLoading: (state, action) => {
      state.moviesByGenresLoading = action.payload;
    },
    setMovies: (state, action) => {
      state.movies = action.payload;
    },
    setMoviesLoading: (state, action) => {
      state.moviesLoading = action.payload;
    },
  },
});

export const {
  setMovies, setMoviesLoading, setMoviesByGenres, setMoviesByGenresLoading,
} = counterSlice.actions;


export const getMoviesByGenres = (genre) => (dispatch) => {
  dispatch(setMoviesByGenresLoading(true));
  apiRequest(`/movies?genres.name=${genre}`, 'GET')
    .then((response) => {
      dispatch(setMoviesByGenresLoading(false));
      dispatch(setMoviesByGenres(response.data));
    }).catch((err) => {
      message.error('Щось пішло не так (не можу взяти фільми)');
      dispatch(setMoviesByGenresLoading(false));
    });
};
export const getMovies = () => (dispatch) => {
  dispatch(setMoviesLoading(true));
  apiRequest('/movies', 'GET')
    .then((response) => {
      dispatch(setMoviesLoading(false));
      dispatch(setMovies(response.data));
    }).catch((err) => {
      message.error('Щось пішло не так (не можу взяти фільми)');
      dispatch(setMoviesLoading(false));
    });
};

export const movies = (state) => state.homePage.movies;
export const moviesLoading = (state) => state.homePage.moviesLoading;
export const moviesByGenres = (state) => state.homePage.moviesByGenres;
export const moviesByGenresLoading = (state) => state.homePage.moviesByGenresLoading;

export default counterSlice.reducer;
