import React, { useState, useRef, useCallback } from 'react';
import {Button, message} from 'antd';
import { createBookingRequest } from "../Booking/bookingSlice";
import CForm from './components/form';
import Card from './components/card';

import './style.scss';
import {useDispatch} from "react-redux";

const initialState = {
  cardNumber: '#### #### #### ####',
  cardHolder: 'FULL NAME',
  cardMonth: '',
  cardYear: '',
  cardCvv: '',
  fcardMonth: false,
  fcardYear: false,
  fcardCvv: false,
  isCardFlipped: false,
};

const Payment = ({ history }) => {
  const [state, setState] = useState(initialState);
  const [load, setLoad] = useState(false);
  const [currentFocusedElm, setCurrentFocusedElm] = useState(null);
  const dispatch = useDispatch();

  const updateStateValues = useCallback(
    (keyName, value) => {
      setState({
        ...state,
        [keyName]: value || initialState[keyName],
        [`f${keyName}`]: true,
      });
    },
    [state],
  );

  // References for the Form Inputs used to focus corresponding inputs.
  const formFieldsRefObj = {
    cardNumber: useRef(),
    cardHolder: useRef(),
    cardDate: useRef(),
    cardCvv: useRef(),
  };

  const focusFormFieldByKey = useCallback((key) => {
    formFieldsRefObj[key].current.focus();
  });

  // This are the references for the Card DIV elements.
  const cardElementsRef = {
    cardNumber: useRef(),
    cardHolder: useRef(),
    cardDate: useRef(),
  };

  const onCardFormInputFocus = (_event, inputName) => {
    const refByName = cardElementsRef[inputName];
    setCurrentFocusedElm(refByName);
  };

  const onCardInputBlur = useCallback(() => {
    setCurrentFocusedElm(null);
  }, []);

  const handleBooking = () =>{
    const { activeTime, placeIds, hallId, userId } = history.location.state;
    setLoad(true)
    dispatch(createBookingRequest(activeTime, placeIds, hallId.hall.id, userId));
    setTimeout(()=>{
      history.push('/profile')
    }, 4000)
  }

  return (
    <div className="wrapper">
      <CForm
        cardMonth={state.cardMonth}
        cardYear={state.cardYear}
        onUpdateState={updateStateValues}
        cardNumberRef={formFieldsRefObj.cardNumber}
        cardHolderRef={formFieldsRefObj.cardHolder}
        cardDateRef={formFieldsRefObj.cardDate}
        onCardInputFocus={onCardFormInputFocus}
        onCardInputBlur={onCardInputBlur}
        fcardMonth={state.fcardMonth}
        fcardYear={state.fcardYear}
        fcardCvv={state.fcardCvv}
        handleBooking={handleBooking}
        load={load}
      >
        <Card
          cardNumber={state.cardNumber}
          cardHolder={state.cardHolder}
          cardMonth={state.cardMonth}
          cardYear={state.cardYear}
          cardCvv={state.cardCvv}
          isCardFlipped={state.isCardFlipped}
          currentFocusedElm={currentFocusedElm}
          onCardElementClick={focusFormFieldByKey}
          cardNumberRef={cardElementsRef.cardNumber}
          cardHolderRef={cardElementsRef.cardHolder}
          cardDateRef={cardElementsRef.cardDate}
        />
      </CForm>

    </div>
  );
};

export default Payment;
